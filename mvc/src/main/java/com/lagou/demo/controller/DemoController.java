package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@LagouController
@LagouRequestMapping("/demo")
@Security({"zhangsan", "lisi"})
public class DemoController {


    @LagouAutowired
    private IDemoService demoService;


    /**
     * URL: /demo/query?name=lisi
     * @param request
     * @param response
     * @param name
     * @return
     */
    @LagouRequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response,String name) throws IOException {
        String result = demoService.get(name);

        response.getWriter().println(result);
        return result;
    }

    @LagouRequestMapping("/test2")
    @Security("zhaowu")
    public String test2(HttpServletRequest request, HttpServletResponse response,String name) throws IOException {
        String result = demoService.get(name);

        response.getWriter().println(result);
        return result;
    }
}
