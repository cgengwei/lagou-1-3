# lagou-1-3

#### 介绍
lagou作业1-3

增加@Security注解

作业中的DemoController加上了@Security({"zhangsan", "lisi"})

```
方法test1（）没有加上@@Security注解
    意味着test1可以用被 张三、李四 访问
    链接是 
    可以访问：
    http://localhost:8080/demo/test1?username=zhangsan
    http://localhost:8080/demo/test1?username=lisi
    不能访问：
    http://localhost:8080/demo/test1?username=zhaowu


方法test2（）加上    @Security("zhaowu")
    意味着test2可以用被 张三、李四、赵五 访问
    可以访问：
    http://localhost:8080/demo/test1?username=zhangsan
    http://localhost:8080/demo/test1?username=lisi
    http://localhost:8080/demo/test1?username=zhaowu
    不能访问
    http://localhost:8080/demo/test1?username=testuser
```